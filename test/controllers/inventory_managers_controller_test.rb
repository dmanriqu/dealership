require 'test_helper'

class InventoryManagersControllerTest < ActionController::TestCase
  setup do
    @inventory_manager = inventory_managers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:inventory_managers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create inventory_manager" do
    assert_difference('InventoryManager.count') do
      post :create, inventory_manager: { iman_fname: @inventory_manager.iman_fname, iman_lname: @inventory_manager.iman_lname }
    end

    assert_redirected_to inventory_manager_path(assigns(:inventory_manager))
  end

  test "should show inventory_manager" do
    get :show, id: @inventory_manager
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @inventory_manager
    assert_response :success
  end

  test "should update inventory_manager" do
    patch :update, id: @inventory_manager, inventory_manager: { iman_fname: @inventory_manager.iman_fname, iman_lname: @inventory_manager.iman_lname }
    assert_redirected_to inventory_manager_path(assigns(:inventory_manager))
  end

  test "should destroy inventory_manager" do
    assert_difference('InventoryManager.count', -1) do
      delete :destroy, id: @inventory_manager
    end

    assert_redirected_to inventory_managers_path
  end
end
