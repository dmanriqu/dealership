require 'test_helper'

class FinanceManagersControllerTest < ActionController::TestCase
  setup do
    @finance_manager = finance_managers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:finance_managers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create finance_manager" do
    assert_difference('FinanceManager.count') do
      post :create, finance_manager: { fman_fname: @finance_manager.fman_fname, fman_lname: @finance_manager.fman_lname }
    end

    assert_redirected_to finance_manager_path(assigns(:finance_manager))
  end

  test "should show finance_manager" do
    get :show, id: @finance_manager
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @finance_manager
    assert_response :success
  end

  test "should update finance_manager" do
    patch :update, id: @finance_manager, finance_manager: { fman_fname: @finance_manager.fman_fname, fman_lname: @finance_manager.fman_lname }
    assert_redirected_to finance_manager_path(assigns(:finance_manager))
  end

  test "should destroy finance_manager" do
    assert_difference('FinanceManager.count', -1) do
      delete :destroy, id: @finance_manager
    end

    assert_redirected_to finance_managers_path
  end
end
