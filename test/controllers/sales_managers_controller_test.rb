require 'test_helper'

class SalesManagersControllerTest < ActionController::TestCase
  setup do
    @sales_manager = sales_managers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sales_managers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sales_manager" do
    assert_difference('SalesManager.count') do
      post :create, sales_manager: { sman: @sales_manager.sman, sman_fname: @sales_manager.sman_fname }
    end

    assert_redirected_to sales_manager_path(assigns(:sales_manager))
  end

  test "should show sales_manager" do
    get :show, id: @sales_manager
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sales_manager
    assert_response :success
  end

  test "should update sales_manager" do
    patch :update, id: @sales_manager, sales_manager: { sman: @sales_manager.sman, sman_fname: @sales_manager.sman_fname }
    assert_redirected_to sales_manager_path(assigns(:sales_manager))
  end

  test "should destroy sales_manager" do
    assert_difference('SalesManager.count', -1) do
      delete :destroy, id: @sales_manager
    end

    assert_redirected_to sales_managers_path
  end
end
