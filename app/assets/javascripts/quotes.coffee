# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#need to wrap my code in ready = -> so that it can execute without me having to reload the page
ready = ->
  price = 0
  tax = 0
  popPrice = ->
    selection = $('#quote_car_id').val()
    $.getJSON '/cars/' + selection + '/car_price', {},(json, response) ->
      $('#quote_quote_price').val(json['car_price']* 1.1)
      price = parseFloat($('#quote_quote_price').val()).toFixed(2)
  $ ->
    $('#quote_car_id').change -> popPrice()

  popTax = ->
    selection2 = $('#quote_car_id').val()
    $.getJSON '/cars/' + selection2 + '/car_price', {},(json, response) ->
      $('#quote_quote_tax').val(json['car_price']* 1.1 * 0.043)
      tax = parseFloat($('#quote_quote_tax').val()).toFixed(2)
  $ ->
    $('#quote_car_id').change -> popTax()

  popTot = ->
    $('#quote_quote_total').val(parseFloat(price) + parseFloat(tax))
  $ ->
    $('#quote_quote_total').bind ('click mouseover mousemove input blur'), -> popTot()

#these two lines below are also required for my code to work without having to reload.
$(document).ready(ready)
$(document).on('page:load', ready)