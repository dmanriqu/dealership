# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

#location.reload()
ready = ->
  $(document).ready ->
    gr = parseFloat($('#gross').val()).toFixed(2)
    ttax = parseFloat($('#total').val()).toFixed(2)
    #alert ttax
    $('#netsales').html("$" + (gr - ttax))

$(document).ready(ready)
$(document).on('page:load', ready)