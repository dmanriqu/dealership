# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
ready = ->
  calculate = ->
    amount = parseFloat($('#amt').val())
    down = parseFloat($('#dp').val())
    i = parseFloat($('#int').val()/100)
    p = amount - down

    $("input[type='radio']").on 'click', ->
    if $('#years_3_Years').is(':checked')
      n = 36
    else if $('#years_4_Years').is(':checked')
      n = 48
    else if $('#years_5_Years').is(':checked')
      n = 60

    #alert n
    x = (1 + (i/12))
    w = Math.pow(x, n)
    #alert w

    result = parseFloat((((i/12) * p * w)/(w - 1))).toFixed(2)
    #result = (amount - down)*i
    $('#resu').val(result)
  $ ->
    $('#calc').bind 'click', calculate


$(document).ready(ready)
$(document).on('page:load', ready)