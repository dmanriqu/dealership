class SalesManagersController < ApplicationController
  before_action :set_sales_manager, only: [:show, :edit, :update, :destroy]

  # GET /sales_managers
  # GET /sales_managers.json
  def index
    @sales_managers = SalesManager.all
  end

  # GET /sales_managers/1
  # GET /sales_managers/1.json
  def show
  end

  # GET /sales_managers/new
  def new
    @sales_manager = SalesManager.new
  end

  # GET /sales_managers/1/edit
  def edit
  end

  # POST /sales_managers
  # POST /sales_managers.json
  def create
    @sales_manager = SalesManager.new(sales_manager_params)

    respond_to do |format|
      if @sales_manager.save
        format.html { redirect_to @sales_manager, notice: 'Sales manager was successfully created.' }
        format.json { render :show, status: :created, location: @sales_manager }
      else
        format.html { render :new }
        format.json { render json: @sales_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales_managers/1
  # PATCH/PUT /sales_managers/1.json
  def update
    respond_to do |format|
      if @sales_manager.update(sales_manager_params)
        format.html { redirect_to @sales_manager, notice: 'Sales manager was successfully updated.' }
        format.json { render :show, status: :ok, location: @sales_manager }
      else
        format.html { render :edit }
        format.json { render json: @sales_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales_managers/1
  # DELETE /sales_managers/1.json
  def destroy
    @sales_manager.destroy
    respond_to do |format|
      format.html { redirect_to sales_managers_url, notice: 'Sales manager was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_manager
      @sales_manager = SalesManager.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sales_manager_params
      params.require(:sales_manager).permit(:sman_fname, :sman_lname)
    end
end
