class FinanceManagersController < ApplicationController
  before_action :set_finance_manager, only: [:show, :edit, :update, :destroy]

  # GET /finance_managers
  # GET /finance_managers.json
  def index
    @finance_managers = FinanceManager.all
  end

  # GET /finance_managers/1
  # GET /finance_managers/1.json
  def show
  end

  # GET /finance_managers/new
  def new
    @finance_manager = FinanceManager.new
  end

  # GET /finance_managers/1/edit
  def edit
  end

  # POST /finance_managers
  # POST /finance_managers.json
  def create
    @finance_manager = FinanceManager.new(finance_manager_params)

    respond_to do |format|
      if @finance_manager.save
        format.html { redirect_to @finance_manager, notice: 'Finance manager was successfully created.' }
        format.json { render :show, status: :created, location: @finance_manager }
      else
        format.html { render :new }
        format.json { render json: @finance_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /finance_managers/1
  # PATCH/PUT /finance_managers/1.json
  def update
    respond_to do |format|
      if @finance_manager.update(finance_manager_params)
        format.html { redirect_to @finance_manager, notice: 'Finance manager was successfully updated.' }
        format.json { render :show, status: :ok, location: @finance_manager }
      else
        format.html { render :edit }
        format.json { render json: @finance_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /finance_managers/1
  # DELETE /finance_managers/1.json
  def destroy
    @finance_manager.destroy
    respond_to do |format|
      format.html { redirect_to finance_managers_url, notice: 'Finance manager was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_finance_manager
      @finance_manager = FinanceManager.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def finance_manager_params
      params.require(:finance_manager).permit(:fman_fname, :fman_lname)
    end
end
