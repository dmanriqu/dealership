class InventoryManagersController < ApplicationController
  before_action :set_inventory_manager, only: [:show, :edit, :update, :destroy]

  # GET /inventory_managers
  # GET /inventory_managers.json
  def index
    @inventory_managers = InventoryManager.all
  end

  # GET /inventory_managers/1
  # GET /inventory_managers/1.json
  def show
  end

  # GET /inventory_managers/new
  def new
    @inventory_manager = InventoryManager.new
  end

  # GET /inventory_managers/1/edit
  def edit
  end

  # POST /inventory_managers
  # POST /inventory_managers.json
  def create
    @inventory_manager = InventoryManager.new(inventory_manager_params)

    respond_to do |format|
      if @inventory_manager.save
        format.html { redirect_to @inventory_manager, notice: 'Inventory manager was successfully created.' }
        format.json { render :show, status: :created, location: @inventory_manager }
      else
        format.html { render :new }
        format.json { render json: @inventory_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inventory_managers/1
  # PATCH/PUT /inventory_managers/1.json
  def update
    respond_to do |format|
      if @inventory_manager.update(inventory_manager_params)
        format.html { redirect_to @inventory_manager, notice: 'Inventory manager was successfully updated.' }
        format.json { render :show, status: :ok, location: @inventory_manager }
      else
        format.html { render :edit }
        format.json { render json: @inventory_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inventory_managers/1
  # DELETE /inventory_managers/1.json
  def destroy
    @inventory_manager.destroy
    respond_to do |format|
      format.html { redirect_to inventory_managers_url, notice: 'Inventory manager was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inventory_manager
      @inventory_manager = InventoryManager.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inventory_manager_params
      params.require(:inventory_manager).permit(:iman_fname, :iman_lname)
    end
end
