json.array!(@cars) do |car|
  json.extract! car, :id, :inventory_manager_id, :car_VIN, :car_make, :car_model, :car_color, :car_price
  json.url car_url(car, format: :json)
end
