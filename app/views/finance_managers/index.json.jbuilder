json.array!(@finance_managers) do |finance_manager|
  json.extract! finance_manager, :id, :fman_fname, :fman_lname
  json.url finance_manager_url(finance_manager, format: :json)
end
