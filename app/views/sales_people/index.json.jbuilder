json.array!(@sales_people) do |sales_person|
  json.extract! sales_person, :id, :sales_manager_id, :sperson_fname, :sperson_lname
  json.url sales_person_url(sales_person, format: :json)
end
