json.array!(@quotes) do |quote|
  json.extract! quote, :id, :car_id, :sales_person_id, :customer_id, :finance_manager_id, :quote_status, :quote_price, :quote_tax, :quote_total
  json.url quote_url(quote, format: :json)
end
