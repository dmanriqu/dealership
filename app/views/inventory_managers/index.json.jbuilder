json.array!(@inventory_managers) do |inventory_manager|
  json.extract! inventory_manager, :id, :iman_fname, :iman_lname
  json.url inventory_manager_url(inventory_manager, format: :json)
end
