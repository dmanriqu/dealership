json.array!(@sales_managers) do |sales_manager|
  json.extract! sales_manager, :id, :sman_fname, :sman
  json.url sales_manager_url(sales_manager, format: :json)
end
