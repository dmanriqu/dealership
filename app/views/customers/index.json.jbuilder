json.array!(@customers) do |customer|
  json.extract! customer, :id, :customer_fname, :customer_lname, :customer_phone, :customer_email
  json.url customer_url(customer, format: :json)
end
