class FinanceManager < ActiveRecord::Base
  has_many :quotes

  def full_name
    "#{fman_fname} #{fman_lname}"
  end
end
