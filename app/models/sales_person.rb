class SalesPerson < ActiveRecord::Base
  belongs_to :sales_manager
  validates_presence_of :sperson_fname, :sperson_lname
end
