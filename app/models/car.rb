class Car < ActiveRecord::Base
  belongs_to :inventory_manager
  has_one :quote
  validates_presence_of :car_VIN, :car_make, :car_model
  paginates_per 10

  def self.search(search, car_VIN, car_model, car_color)
    if search
      where('car_VIN LIKE ? OR car_model LIKE ? OR car_color LIKE ?', "%#{search}%", "%#{search}%", "%#{search}%")
    else
      where(nil)
    end
  end
end
