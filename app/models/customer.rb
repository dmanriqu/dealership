class Customer < ActiveRecord::Base
  has_many :quotes
  validates_presence_of :customer_fname, :customer_lname, :customer_phone
  validates_format_of :customer_phone, with: /\A\(\d{3}\) \d{3}-\d{4}\z/, message: "Allowed format: (###) ###-#### format"
  paginates_per 10

  def self.search(search)
    if search
      where('customer_fname LIKE ?', "%#{search}%")
    else
      where(nil)
    end
  end

  def full_name
    "#{customer_fname} #{customer_lname}"
  end

end
