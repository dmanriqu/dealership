class Quote < ActiveRecord::Base
  belongs_to :finance_manager
  belongs_to :sales_person
  belongs_to :customer
  belongs_to :car
  validates_presence_of :quote_price, :quote_tax, :quote_total

  def self.calc_gross_profit(quotes)
   Quote.where(:quote_status => true).sum(:quote_total)
  end

  def self.calc_total_tax(quotes)
    Quote.where(:quote_status => true).sum(:quote_tax)
  end

  def self.calc_net_sales(quotes)
    Quote.where(:quote_status => true).sum(:quote_total)
  end
end
