# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Car.delete_all
Customer.delete_all
FinanceManager.delete_all
InventoryManager.delete_all
Quote.delete_all
SalesManager.delete_all
SalesPerson.delete_all


InventoryManager.create(iman_fname: "Vanessa", iman_lname: "Steins")

SalesManager.create(sman_fname: "Felix", sman_lname: "Vazquez")

SalesPerson.create(sales_manager_id: 1, sperson_fname: "Sam", sperson_lname: "Jonhson")
SalesPerson.create(sales_manager_id: 1, sperson_fname: "Tania", sperson_lname: "Rodriguez")

Car.create(inventory_manager_id: 1, car_VIN: "1GCEK14T94Z192501", car_make: "Toyota", car_model: "Isis Platana", car_color: "Yellow", car_price: 29213.21)
Car.create(inventory_manager_id: 1, car_VIN: "4TAWM72N8XZ213408", car_make: "Hyundai", car_model: "Elantra", car_color: "Maroon", car_price: 15322.00)
Car.create(inventory_manager_id: 1, car_VIN: "SCA2D68508U567871", car_make: "Toyota", car_model: "Yaris", car_color: "Silver", car_price: 13430.00)
Car.create(inventory_manager_id: 1, car_VIN: "1N6AA07B25N328603", car_make: "Hyundai", car_model: "I10", car_color: "Black", car_price: 10200.99)
Car.create(inventory_manager_id: 1, car_VIN: "JH4CL958X5C460276", car_make: "Hyundai", car_model: "Genesis", car_color: "Blue", car_price: 34988.00)
Car.create(inventory_manager_id: 1, car_VIN: "1G8AN18F67Z448022", car_make: "Honda", car_model: "Pilot", car_color: "Pink", car_price: 32001.00)
Car.create(inventory_manager_id: 1, car_VIN: "2C4GP54L11R568471", car_make: "Honda", car_model: "Civic", car_color: "Red", car_price: 17323.42)
Car.create(inventory_manager_id: 1, car_VIN: "JM1FE1T49B0535623", car_make: "Jeep", car_model: "Cherokee", car_color: "White", car_price: 24238.00)
Car.create(inventory_manager_id: 1, car_VIN: "WP0CA299X3S962306", car_make: "Jeep", car_model: "Wrangler", car_color: "White", car_price: 40786.63)
Car.create(inventory_manager_id: 1, car_VIN: "2C3HD36JX1H243180", car_make: "Jeep", car_model: "Grand Cherokee", car_color: "Azure", car_price: 45699.00)
Car.create(inventory_manager_id: 1, car_VIN: "1GNEC13R8YR046297", car_make: "Dodge", car_model: "Challenger", car_color: "Light blue", car_price: 30981.44)
Car.create(inventory_manager_id: 1, car_VIN: "WVWBB71K08W440255", car_make: "Dodge", car_model: "Charger", car_color: "Orange", car_price: 25983.00)
Car.create(inventory_manager_id: 1, car_VIN: "1D8HD58D14F364203", car_make: "Chrysler", car_model: "200", car_color: "Black", car_price: 20732.21)
Car.create(inventory_manager_id: 1, car_VIN: "1GNFG15X461020795", car_make: "Chrysler", car_model: "300c", car_color: "Platinum", car_price: 34090.09)
Car.create(inventory_manager_id: 1, car_VIN: "1FTWF33R09E951316", car_make: "Nissan", car_model: "Sentra", car_color: "Tan", car_price: 15983.00)
Car.create(inventory_manager_id: 1, car_VIN: "1GNFG15T041631241", car_make: "Nissan", car_model: "Rogue", car_color: "Plum", car_price: 26931.03)
Car.create(inventory_manager_id: 1, car_VIN: "2S3TD52V0Y6963217", car_make: "Nissan", car_model: "Murano", car_color: "Red", car_price: 29123.92)
Car.create(inventory_manager_id: 1, car_VIN: "1FTWX30548E545312", car_make: "Ford", car_model: "Fiesta", car_color: "Black", car_price: 12553.12)
Car.create(inventory_manager_id: 1, car_VIN: "1B7GL32X32S100375", car_make: "Ford", car_model: "Focus", car_color: "Yellow", car_price: 16332.66)
Car.create(inventory_manager_id: 1, car_VIN: "1FMCU01151K899171", car_make: "Ford", car_model: "Fusion", car_color: "White", car_price: 19745.00)


Customer.create(customer_fname: "Dolan", customer_lname: "Davidson", customer_phone: "(123) 312-3312", customer_email: "doland@testing.cxx")
Customer.create(customer_fname: "Stuart", customer_lname: "Little", customer_phone: "(412) 523-3312", customer_email: "st@gmail.cxx")
Customer.create(customer_fname: "Thomas", customer_lname: "Thecat", customer_phone: "(622) 866-3312", customer_email: "gatunox100pre@hotmail.cxx")
Customer.create(customer_fname: "Luis", customer_lname: "Huayta", customer_phone: "(442) 621-3312", customer_email: "lucho21@gmail.cxx")
Customer.create(customer_fname: "Camaco", customer_lname: "Weberto", customer_phone: "(421) 743-3312", customer_email: "weberto1000@niconico.cxx")
Customer.create(customer_fname: "Tiburcio", customer_lname: "Huamani", customer_phone: "(534) 775-3312", customer_email: "tiburon@tangelfire.cxx")
Customer.create(customer_fname: "Carlos", customer_lname: "Munoz", customer_phone: "(431) 645-3312", customer_email: "cmun@testing.cxx")
Customer.create(customer_fname: "David", customer_lname: "Martinez", customer_phone: "(521) 882-3312", customer_email: "dmarti@aol.cxx")

FinanceManager.create(fman_fname: "Joseph", fman_lname: "Donovan")

#Sales tax is 4.3% of sale price
Quote.create(car_id: 1, sales_person_id: 2, customer_id: 3, finance_manager_id: 1, quote_status: false, quote_price: 29213.21, quote_tax: 1256.17, quote_total: 30469.38)
Quote.create(car_id: 3, sales_person_id: 1, customer_id: 6, finance_manager_id: 1, quote_status: true, quote_price: 13430.00, quote_tax: 577.49, quote_total: 14007.49)