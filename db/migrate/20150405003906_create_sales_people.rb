class CreateSalesPeople < ActiveRecord::Migration
  def change
    create_table :sales_people do |t|
      t.integer :sales_manager_id
      t.string :sperson_fname
      t.string :sperson_lname

      t.timestamps null: false
    end
  end
end
