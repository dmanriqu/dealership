class CreateSalesManagers < ActiveRecord::Migration
  def change
    create_table :sales_managers do |t|
      t.string :sman_fname
      t.string :sman_lname

      t.timestamps null: false
    end
  end
end
