class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.integer :inventory_manager_id
      t.string :car_VIN
      t.string :car_make
      t.string :car_model
      t.string :car_color
      t.decimal :car_price

      t.timestamps null: false
    end
  end
end
