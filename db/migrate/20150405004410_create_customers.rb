class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :customer_fname
      t.string :customer_lname
      t.string :customer_phone
      t.string :customer_email

      t.timestamps null: false
    end
  end
end
