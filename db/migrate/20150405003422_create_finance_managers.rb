class CreateFinanceManagers < ActiveRecord::Migration
  def change
    create_table :finance_managers do |t|
      t.string :fman_fname
      t.string :fman_lname

      t.timestamps null: false
    end
  end
end
