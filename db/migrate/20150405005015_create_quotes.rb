class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :car_id
      t.integer :sales_person_id
      t.integer :customer_id
      t.integer :finance_manager_id
      t.boolean :quote_status
      t.decimal :quote_price
      t.decimal :quote_tax
      t.decimal :quote_total

      t.timestamps null: false
    end
  end
end
