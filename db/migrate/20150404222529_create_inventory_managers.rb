class CreateInventoryManagers < ActiveRecord::Migration
  def change
    create_table :inventory_managers do |t|
      t.string :iman_fname
      t.string :iman_lname

      t.timestamps null: false
    end
  end
end
