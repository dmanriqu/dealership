# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150405005015) do

  create_table "cars", force: :cascade do |t|
    t.integer  "inventory_manager_id"
    t.string   "car_VIN"
    t.string   "car_make"
    t.string   "car_model"
    t.string   "car_color"
    t.decimal  "car_price"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "customers", force: :cascade do |t|
    t.string   "customer_fname"
    t.string   "customer_lname"
    t.string   "customer_phone"
    t.string   "customer_email"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "finance_managers", force: :cascade do |t|
    t.string   "fman_fname"
    t.string   "fman_lname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "inventory_managers", force: :cascade do |t|
    t.string   "iman_fname"
    t.string   "iman_lname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "quotes", force: :cascade do |t|
    t.integer  "car_id"
    t.integer  "sales_person_id"
    t.integer  "customer_id"
    t.integer  "finance_manager_id"
    t.boolean  "quote_status"
    t.decimal  "quote_price"
    t.decimal  "quote_tax"
    t.decimal  "quote_total"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "sales_managers", force: :cascade do |t|
    t.string   "sman_fname"
    t.string   "sman_lname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sales_people", force: :cascade do |t|
    t.integer  "sales_manager_id"
    t.string   "sperson_fname"
    t.string   "sperson_lname"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

end
